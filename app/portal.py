import http
import logging
import ssl
import urllib
from datetime import datetime

from django.conf import settings


class Portal(object):

    @classmethod
    def create_dcl_account(cls, name, email, account_name, domain_id, domain_name):

        logging.info(
            f"Creating DCL account for {account_name} and "
            f"DCL user for {name} ({email}). Domain: {domain_name} (ID: {domain_id})."
        )
        url = settings.DCL["hostname"]
        uri = settings.DCL["uri"]
        params = {
            "name": name[:50],
            "email": email,
            "companyName": account_name[:50],
            "primaryPhone": "1",
            "secondaryPhone": "",
            "address": "",
            "city": "",
            "stateProvince": "",
            "postalZip": "",
            "domainId": domain_id,
            "domainName": domain_name[:50],
        }
        try:
            response = cls.post(
                url=url,
                uri=uri,
                params=params,
                exception_message="DCL Account",
                verify=False,
                headers=settings.DCL["headers"],
            )
            data = response.read()
            assert response.status == 200
            assert data == b"Account Created"
        except AssertionError as e:
            logging.exception(
                "support.broadsign.com has been reached but DCL account "
                f"creation of {account_name} failed because the response is not expected. "
                f"{str(e)} data = {data}"
            )
            return False
        except Exception as e:
            logging.exception(
                f"DCL account creation of {account_name} failed for some unaccounted reason: "
                f"{str(e)}"
            )
            return False
        else:
            return True

    @classmethod
    def create_portal_user(cls, name, email, password):
        logging.info(f"Creating Portal user for {email}.")
        url = settings.PORTAL["hostname"]
        uri = settings.PORTAL["uri"]
        params = {
            "name": name,
            "username": email,
            "email": email,
            "password": password,
            "password2": password,
        }
        try:
            response = cls.post(
                url=url,
                uri=uri,
                params=params,
                headers=settings.DCL["headers"],
                exception_message="Portal User",
            )
            assert response.status == 200
        except AssertionError as e:
            logging.exception(
                "portal.broadsign.com has been reached but the Portal "
                f"user of {email} could not be created. " + str(e)
            )
            return False
        except Exception as e:
            logging.exception(
                f"The creation of the portal user of {email} could not "
                "be completed for an unaccounted reason. " + str(e)
            )
            return False
        else:
            return True

    @staticmethod
    def post(url, uri, params, headers, exception_message, verify=True):

        params = urllib.parse.urlencode(params)
        if not verify:
            connection = http.client.HTTPSConnection(
                url, context=ssl._create_unverified_context()
            )
        else:
            connection = http.client.HTTPSConnection(url)
        connection.request("POST", uri, params, headers=headers)
        response = connection.getresponse()
        return response
