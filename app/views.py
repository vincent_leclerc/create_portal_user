import json
import logging
from ast import literal_eval
from datetime import datetime

from django.core.exceptions import ValidationError
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from app.forms import PortalForm
from app.portal import Portal


@csrf_exempt
def create_portal_user(request):

    if request.method == "POST":
        decoded_body = request.body.decode("utf-8")
        if decoded_body == '':
            json_body = {}
        else:
            logging.debug(decoded_body)
            json_body = json.loads(decoded_body)
        try:
            data = PortalForm(json_body).process()
        except ValidationError as e:
            error_json = literal_eval(str(e))
            logging.error(str(error_json))
            return JsonResponse({"error": dict(literal_eval(str(e)))})
        name = " ".join([data["first_name"], data["last_name"]])
        portal_user_created = False
        dcl_account_created = Portal.create_dcl_account(
            name=name,
            email=data["email"],
            account_name=data["account_name"],
            domain_id=int(data["domain_id"]),
            domain_name=data["domain_name"],
        )
        if dcl_account_created:
            portal_user_created = Portal.create_portal_user(
                name=name, email=data["email"], password=data["password"]
            )

        return JsonResponse(
            {"dcl": dcl_account_created, "portal": portal_user_created}, status=200
        )
    else:
        return JsonResponse(
            {"error": "This method isn't allowed for this URI."}, status=405
        )
