from unittest.mock import Mock, patch
from urllib.error import HTTPError
from urllib.request import Request

from django.conf import settings
from django.test import TestCase

from app.portal import Portal


class PortalTest(TestCase):
    @patch("app.portal.Portal.post")
    def test_create_dcl_account_success(self, mock_post):
        mock_post.return_value.read.return_value = b"Account Created"
        mock_post.return_value.status = 200
        response = Portal.create_dcl_account(
            "Célestin Laperrière",
            "celestin@gmail.com",
            "Derrière cette grange",
            123,
            "grange",
        )
        self.assertTrue(response)

    @patch("app.portal.Portal.post")
    def test_create_dcl_account_failure(self, mock_post):
        mock_post.return_value.status.return_value = 401
        response = Portal.create_dcl_account(
            "Célestin Laperrière",
            "celestin@gmail.com",
            "Derrière cette grange",
            123,
            "grange",
        )
        self.assertRaises(AssertionError)

    @patch("app.portal.Portal.post")
    def test_create_dcl_account_unknown_failure(self, mock_post):
        url = "https://" + settings.PORTAL["hostname"]
        mock_post.return_value.status = 200
        mock_post.raiseError.side_effect = Exception("Unit Testing error.")
        Portal.create_dcl_account(
            "Célestin Laperrière",
            "celestin@gmail.com",
            "Derrière cette grange",
            123,
            "grange",
        )
        self.assertRaises(Exception, "An exception has been properly raised.")

    @patch("app.portal.Portal.post")
    def test_create_portal_user_success(self, mock_post):
        mock_post.return_value.status = 200
        response = Portal.create_portal_user(
            "Célestin Laperrière", "celestin2@gmail.com", "qwerty147!"
        )
        self.assertTrue(response)

    @patch("app.portal.Portal.post")
    def test_create_portal_user_failure(self, mock_post):
        mock_post.return_value.status = 404
        response = Portal.create_portal_user(
            "Célestin Laperrière", "celestin2@gmail.com", "qwerty147!"
        )
        self.assertRaises(AssertionError)

    @patch("app.portal.Portal.post")
    def test_create_portal_user_unknown_error(self, mock_post):
        mock_post.return_value.status = 200
        mock_post.side_effect = Exception("Unit Testing error.")
        response = Portal.create_portal_user(
            "Célestin Laperrière", "celestin2@gmail.com", "qwerty147!"
        )
        self.assertRaises(Exception)
