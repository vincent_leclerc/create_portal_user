import json
import time

from django.test import TestCase
from django.urls import reverse


class ViewsTest(TestCase):
    def test_create_portal_user_wrong_method(self):
        response = self.client.get(reverse("create_portal_user"))
        self.assertEqual(405, response.status_code)

    def test_create_portal_user_no_data(self):
        data = {
            "first_name": "Test",
            "last_name": "Free Trial",
            "email": f"test_free_trial+{str(time.time())}@broadsign.com",
            "password": "NkTKN9doPAz3MbtkHgwS",
            "account_name": "Test Free Trial",
            "domain_name": "test_free_trial",
            "domain_id": 123465789,
        }
        print(data["email"])
        print(
            vars(
                self.client.post(
                    path=reverse("create_portal_user"),
                    data=json.dumps(data),
                    content_type="text/json",
                    headers={},
                )
            )
        )
