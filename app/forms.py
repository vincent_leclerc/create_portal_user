from django import forms
from django.core.exceptions import ValidationError


class PortalForm(forms.Form):
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.CharField()
    password = forms.CharField()
    account_name = forms.CharField()
    domain_id = forms.CharField()
    domain_name = forms.CharField()

    def process(self):
        if not self.is_valid():
            raise ValidationError(dict(self.errors.items()))
        data = self.cleaned_data
        return data
