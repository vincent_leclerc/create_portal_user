from django.conf.urls import url

from . import views

urlpatterns = [
    url(r"^create-portal-user/?$", views.create_portal_user, name="create_portal_user")
]
